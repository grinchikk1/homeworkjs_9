"use strict";

function arrayList(arr, parent = document.body) {
  const ul = document.createElement("ul");
  parent.appendChild(ul);
  arr.forEach((item) => {
    const li = document.createElement("li");
    if (Array.isArray(item)) {
      arrayList(item, li);
    } else {
      li.textContent = item;
    }
    ul.appendChild(li);
  });
}

// setTimeout(() => {
//   document.body.innerHTML = '';
// }, 3000);

let seconds = 3;
const timerEl = document.createElement('div');
document.body.appendChild(timerEl);

const intervalId = setInterval(() => {
  seconds--;
  timerEl.textContent = `Сторінка буде очищена через ${seconds} сек.`;
  if (seconds <= 0) {
    clearInterval(intervalId);
    document.body.innerHTML = '';
  }
}, 1000);

// arrayList(["1", "2", "3", "sea", "user", 23]);
// arrayList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
arrayList([
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
]);
